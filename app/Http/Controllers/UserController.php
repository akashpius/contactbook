<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Users;
use Carbon\Carbon;

class UserController extends Controller
{
    public function createUser(Request $request){
        $user = new Users;
        $data = [
            'name' => $request->name,
            'mobile' => $request->mobile,
            'status' => [
                'status' => 'active',
                'date' => Carbon::now()->timestamp
            ]
        ];
        $userSaved = $user->add($data);
        if($userSaved){
            return response($content = json_encode(array('Message' => "Saved Successfully", 'Data' => $userSaved)), $status = 200);
        }else{
            return response($content = json_encode(array('Message' => "Saved Failed")), $status = 400);
        }
    }
}
