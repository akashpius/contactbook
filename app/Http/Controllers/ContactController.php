<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Contacts;
use App\Model\Users;
use Carbon\Carbon;

class ContactController extends Controller
{
    public function createContact(Request $request){
        $contact = new Contacts;
        $data = [
            'name' => $request->name,
            'contact' => $request->contact,
            'status' => [
                'status' => 'active',
                'date' => Carbon::now()->timestamp
            ]
        ];
        $contactSaved = $contact->add($data);
        if($contactSaved){
            return response($content = json_encode(array('Message' => "Saved Successfully", 'Data' => $contactSaved)), $status = 200);
        }else{
            return response($content = json_encode(array('Message' => "Saved Failed")), $status = 400);
        }
    }

    public function listContacts(Request $request){
        $contact = new Contacts;
        $user = new Users;
        $contactSaved = Contacts::all()->toArray();
        $contactsList = array();
        foreach ($contactSaved as $key => $value) {
            foreach ($value['contact'] as $contactKey => $contactValue) {
                if($user->findOne(["mobile" => $contactValue], '*')){
                    $value['exist'] = "true";
                }
            }
            array_push($contactsList,$value);
        }
        if($contactsList){
            return response($content = json_encode(array('Message' => "Listed Successfully", 'Data' => $contactsList)), $status = 200);
        }else{
            return response($content = json_encode(array('Message' => "Listed Failed")), $status = 400);
        }
    }

}
