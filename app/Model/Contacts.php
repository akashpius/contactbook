<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Contacts extends Eloquent
{
    protected $collection = "contacts";

    function __construct(){
        $this->connection = env('DB_CONNECTION');
    }

    public function add($data){
        foreach ($data as $key => $value) :
            $this->$key = $value;
        endforeach;
        try {
            $this->save();
            return $this;
        } catch (Exception $ex) {
            return false;
        }
    }
}
