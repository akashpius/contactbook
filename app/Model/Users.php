<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use DB;

class Users extends Eloquent
{
    protected $collection = "users";

    function __construct(){
        $this->connection = env('DB_CONNECTION');
    }

    public function add($data){
        $user  = new Users;
        $users = $user->findOne(["mobile" => $data['mobile']], '*');
        if(!$users){
            foreach ($data as $key => $value) :
                $this->$key = $value;
            endforeach;
            try {
                $this->save();
                return $this;
            } catch (Exception $ex) {
                return false;
            }
        }else{
            return false;
        }
        
    }

    public function findOne($filters, $parameters){
        $data = DB::table($this->collection)->select($parameters)->where(key($filters), $filters[key($filters)])->first();
        if ($data) {
            return $data;
        } else {
            return false;
        }
    }
    
}
