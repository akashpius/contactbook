After cloning the source code, please run "composer install" command for installing laravel and the essential packages.

I have used mongoDB as the database and PHP 7.0.

In total I have created 3 api. one for creating user in system, another for creating contact in system and the last one for listing the contacts in system. As mentioned in the test if the contacts number exist as an user in the system a flag with name "exist" and value "true" is returned with the list of contacts.

Please check the postman collection for the body structures and api urls I have used. 